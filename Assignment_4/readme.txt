In this assignment, you will implement an attack against basic CBC-MAC showing that
basic CBC-MAC is not secure when used to authenticate/verify messages of different lengths.
Here, you will be given the ability to obtain tags (with respect to some unknown key)
for any 2-block (32-byte) messages of your choice; your goal is to forge a valid tag
(with respect to the same key) on the 4-block (64-byte) message:
"I, the server, hereby agree that I will pay $100 to this student".
(You should verify that the message contains exactly 64 ASCII characters.)
You will also be given access to a verification routine that you can use to verify your solution.
Note that I did not explicitly describe how to implement this attack in the lectures.
To carry out this attack, you will have to use your knowledge of how basic CBC-MAC works, plus a little creativity...
All the files needed for this assignment are available here, including a README file that should explain everything.
This assignment requires the ability to perform basic networking. Because we do not
assume students necessarily know this, we have provided stub code for doing basic
networking in C, Ruby, and Python, but you are welcome to use any language of your
choice as long as you are able to write code for basic networking functionality in that language.
(Students may feel free to post stub code in other languages for the networking component on the discussion boards.)
