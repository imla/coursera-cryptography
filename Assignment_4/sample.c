#include "oracle.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <string.h>
#include <assert.h>

#define MAX_LENGTH 64
#define BLOCK_LENGTH 16

static void xor_blocks(unsigned char* a, const unsigned char* b) {
    int i;
    for(i = 0; i < BLOCK_LENGTH; ++i)
        a[i] ^= b[i];
}

int main(int argc, char *argv[]) {
    unsigned char message[MAX_LENGTH];
    unsigned char tag[BLOCK_LENGTH];

    if (argc != 2) {
        printf("Usage: sample <filename>\n");
        return -1;
    }

    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        perror("can't open file");
        return -1;
    }
    int i;
    for(i = 0; i < MAX_LENGTH; i++)
      if (fscanf(fp, "%c", &message[i]) == EOF)
        break;

    fclose(fp);
    assert(i == MAX_LENGTH);

    unsigned char buf[64];
    memcpy(buf, message, 64 * sizeof(unsigned char));

    if(Oracle_Connect())
        return -1;

    // get tag for blocks{1,2}
    Mac(buf, 2 * BLOCK_LENGTH, tag);
    xor_blocks(&buf[2 * BLOCK_LENGTH], tag);// remove this tag from the message block3

    // get target tag for blocks{1,2,3,4} of the real message.
    Mac(&buf[2 * BLOCK_LENGTH], 2 * BLOCK_LENGTH, tag);

    // send message (of length 64) and tag to be verified
    int ret = Vrfy(message, i, tag);

    Oracle_Disconnect();

    if (ret) {
        printf("Message verified successfully!\nThe tag is:\n");
        for (i = 0; i < BLOCK_LENGTH; ++i)
            printf("%2X", tag[i]);
        printf("\n");
    }
    else
        printf("Message verficiation failed.\n");

}
/* Solution:

*/
