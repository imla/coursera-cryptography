#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <string.h>

#ifdef NO_NET
    #define Oracle_Connect() (0)
    #define Oracle_Disconnect() {}
    #define Oracle_Send cbcdec
    int cbcdec (unsigned char *, int);
#else
    #include "oracle.h"
#endif


static int find_padding(const unsigned char* ci)
{
    unsigned char buf[16*2];
    memcpy(buf, ci, 16*2*sizeof(unsigned char));

    int i;
    for (i = 0; i < 16; ++i){
        buf[i] = 0xAA;
        if (Oracle_Send(buf, 2) == 0)
            break;
    }

    return i;
}

static int attack(  const unsigned char* ci,
                    unsigned char* const pi,
                    const int ii)
{
    unsigned char buf[16*2];
    memcpy(buf, ci, 16*2*sizeof(unsigned char));

    int i;
    for (i = ii; i > 0; --i){
        int m = 16-i; // this is the current padding
        int m1 = m+1; // next
        m ^= m1;      // so I must remove this

        int j;
        for (j = i; j<16; ++j){
            buf[j] ^= m;
        }

        for (j = 0; j < 255; ++j){
            buf[i-1] = j;
            if (Oracle_Send(buf, 2) != 0)
                break; // change j until we get m1;
        }
        pi[i-1] = j ^ ci[i-1] ^ m1; // remove m1 and j
        printf("found: 0x%02X : %c\n", pi[i-1], pi[i-1]);
    }
    return 0;
}


int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("Usage: sample <filename>\n");
        return -1;
    }

    unsigned char ctext[16*3];
    unsigned char ptext[16*2+1];

    FILE *fpIn = fopen(argv[1], "r");
    if (fpIn){
        int i, tmp;
        for(i=0; i<16*3; i++) {
            fscanf(fpIn, "%02x", &tmp);
            ctext[i] = tmp;
        }
        fclose(fpIn);
    }
    else {
        fprintf(stderr, "error: %s: can't be opened!\n", argv[1]);
        return -1;
    }

    printf("Please wait!\n");
    if(Oracle_Connect() != 0)
        return -1;

    //TODO move find_padding to attack() and loop for all blocks
    int i = find_padding(ctext+16);
    printf("got padding @%i, value:%i, length of message:%i\n", i, 16-i, 16+i);
    ptext[16+i] = 0;
    attack(ctext+16, ptext+16, i);
    attack(ctext, ptext, 16);
    Oracle_Disconnect();

    printf("ptext:\n%s\n", ptext);
}
/* Solution:
Yay! You get an A. =)
*/
