#ifndef HEADER_ORACLE_H
# define HEADER_ORACLE_H

#include <openssl/bn.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 *
 */

int Oracle_Connect();
int Oracle_Disconnect();

BIGNUM *Sign(const BIGNUM *m);
int Verify(const BIGNUM *m, const BIGNUM *sigma);

#ifdef __cplusplus
}
#endif
#endif
