#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <openssl/bn.h>


const int MAX_PACKET_LEN        = 8192;
const int NOT_BINARY_STR_ERR    = -1;
const int MISSING_DELIMITER_ERR = -2;

int signfd, vrfyfd;

int Oracle_Connect() {
    struct sockaddr_in sign_servaddr, vrfy_servaddr;

    signfd = socket(AF_INET, SOCK_STREAM, 0);
    vrfyfd = socket(AF_INET, SOCK_STREAM, 0);

    bzero(&sign_servaddr, sizeof(sign_servaddr));
    bzero(&vrfy_servaddr, sizeof(vrfy_servaddr));

    sign_servaddr.sin_family = AF_INET;
    vrfy_servaddr.sin_family = AF_INET;
    sign_servaddr.sin_addr.s_addr=inet_addr("54.165.60.84");
    vrfy_servaddr.sin_addr.s_addr=inet_addr("54.165.60.84");
    sign_servaddr.sin_port=htons(8080);
    vrfy_servaddr.sin_port=htons(8081);

    if( !connect(signfd, (struct sockaddr *)&sign_servaddr, sizeof(sign_servaddr)) &
        !connect(vrfyfd, (struct sockaddr *)&vrfy_servaddr, sizeof(vrfy_servaddr)) ) {
        printf("Connected to server successfully.\n");
        return 0;
    } else {
        perror("Failed to connect to oracle");
        return -1;
    }
}

int Oracle_Disconnect() {
    if(!close(signfd) && !close(vrfyfd)) {
        printf("Connection closed successfully.\n");
        return 0;
    } else {
        perror("[WARNING]: You haven't connected to the server yet");
        return -1;
    }
}

// out = bin m || delimiter
static void m_delimiter(const BIGNUM *m, const char delimiter, char *out){
    int len = BN_num_bits(m);
    if (len == 0) len++;// to send at least '0'||delimiter
    int i;
    for (i = 0; i < len; ++i)
        out[len-i-1] = BN_is_bit_set(m, i)? '1' : '0';

    out[len] = delimiter;
}

BIGNUM* Sign(const BIGNUM* m){
    BIGNUM *sigma = BN_new();
    int len = BN_num_bits(m);
    if (len == 0) len++;// room for '0'
    if (len > 504)
        fprintf (stderr, "error: The message is bigger than 504 bits\n");

    if (BN_is_negative(m))
        fprintf (stderr, "error: Message cannot be negative!\n");

    {// send bin m || 'X'
        char out[len+1];
        m_delimiter(m, 'X', out);
        if(0 > send(signfd, out, len+1, 0)) {
            perror("error: You haven't connected to the server yet");
            return sigma;
        }
    }

    {// receive sigma in bin byte form or "-3" on error
        char in[MAX_PACKET_LEN];
        const int len = recv(signfd, in, MAX_PACKET_LEN, 0);
        if(len < 0) {
            perror("[ERROR]: Recv failed");
            return sigma;
        }
        if (in[0] == '-' && in[1] == '3') {
            fprintf(stderr, "error: You cannot request a signature on the original message!\n");
            return sigma;
        }
        //ensure that sigma has enough space for len bits
        sigma = bn_expand(sigma, len);
        if (sigma == NULL) {
            perror("error: out of memory");
            return NULL;
        }
        int i;
        for (i = 0; i < len; ++i)
            if (in[len-i-1] == '1') BN_set_bit(sigma, i);

        printf("info: Sign worked\n");
        return sigma;
    }
}

int Verify(const BIGNUM* m, const BIGNUM* sigma){
    int len_m = BN_num_bits(m);
    if (len_m == 0) len_m++;
    if (len_m > 504) // I think the server will be happy
        fprintf(stderr, "error: The message is bigger than 504 bits\n");

    int len_sigma = BN_num_bits(sigma);
    if (len_sigma == 0) len_sigma++;

    if (BN_is_negative(m) || BN_is_negative(sigma)) {
        fprintf (stderr, "error: Message and signature cannot be negative!\n");
        return 0;
    }

    {//send bin m || ':' || bin sigma || 'X'
        const int len = len_m+1 + len_sigma+1;
        char out[len];
        m_delimiter(m, ':', out); m_delimiter(sigma, 'X', &out[len_m+1]);
        if( 0 > send(vrfyfd, out, len, 0)) {
            perror("[WARNING]: You haven't connected to the server yet");
            return 0;
        }
    }

    {//receive one of string "0", "1" and probably not "-1", "-2"
        char in[2+1];
        const int len = recv(vrfyfd, in, 2, 0);
        if(len < 0) {
            perror("error: Recv failed");
            return 0;
        }
        in[len] = 0;
        int i = atoi(in);
        if (i == NOT_BINARY_STR_ERR) {
            fprintf(stderr, "error: Message and/or signature were not valid binary strings.\n");
            return 0;
        }
        if (i == MISSING_DELIMITER_ERR) {
            fprintf(stderr, "error: Missing delimiter between message and signature.\n");
            return 0;
        }
        printf("info: Verify worked %s\n", in);
        return i;
    }
}


