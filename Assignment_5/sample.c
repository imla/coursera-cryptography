#include <stdio.h>
#include <string.h>

#include "oracle.h"

/*
 * M     = 0..0m | 0..0m
 *       = m . (1+2⁵¹²)
 *
 * S(m)  = M^d mod n
 *       = m^d . (1+2⁵¹²)^d mod n
 * S(m.a)= m^d . a^d . (1+2⁵¹²)^d mod n  //log2(m.a) <= 504
 *
 * if a can be writen as a = x . y-¹ then:
 * S(m.a)= m^d . x^d . y-¹^d . (1+2⁵¹²)^d mod n
 * S(x)  = x^d . (1+2⁵¹²)^d mod n        //log2(x) <= 504
 * S(y)  = y^d . (1+2⁵¹²)^d mod n        //log2(y) <= 504
 *
 * if you look from right to left you can get this formula.
 * (S(x)-¹ . S(m.a)) . S(y) = (m^d . y-¹^d) . S(y)
 *                          =  m^d . (1+2⁵¹²)^d mod n
 *                          = S(m)
 * the simplest choice is x = 2 and y = 1
 */

const char* s_n = "119077393994976313358209514872004186781083638474007212865571534799455802984783764695504518716476645854434703350542987348935664430222174597252144205891641172082602942313168180100366024600206994820541840725743590501646516068078269875871068596540116450747659687492528762004294694507524718065820838211568885027869";
const char* msg = "Crypto is hard --- even schemes that look complex can be broken";

int main(){

    if (Oracle_Connect())
        return -1;

    BIGNUM *n = BN_new(); BN_dec2bn(&n, s_n);
    BIGNUM *m = BN_bin2bn(msg, strlen(msg), NULL);
    //m2 = m.2
    BIGNUM *m2  = BN_new(); BN_lshift1(m2, m);
    BIGNUM *One = BN_new(); BN_set_word(One, 1);
    BIGNUM *Two = BN_new(); BN_set_word(Two, 2);

    BIGNUM *sigma_m2 = Sign(m2);
    BIGNUM *sigma_1  = Sign(One);
    BIGNUM *sigma_2  = Sign(Two);

    BN_CTX* ctx = BN_CTX_new();
    BIGNUM *sigma_m = BN_mod_inverse(NULL, sigma_2, n, ctx);//S(m)= S(2)-¹
    BN_mod_mul(sigma_m, sigma_m, sigma_m2, n, ctx);//S(m) = S(2)-¹.S(m.2)
    BN_mod_mul(sigma_m, sigma_m,  sigma_1, n, ctx);//S(m) = S(2)-¹.S(m.2).S(1)

    if (Verify(m, sigma_m))
        BN_print_fp(stdout, sigma_m);

    Oracle_Disconnect();

    BN_free(n);
    BN_free(m); BN_free(sigma_m);
    BN_free(m2); BN_free(sigma_m2);
    BN_free(One); BN_free(sigma_1);
    BN_free(Two); BN_free(sigma_2);
    BN_CTX_free(ctx);

    return 0;
}
/* Solution:
68966743101cb086bde78df31c1ded2900ea9155914317ec5babb47966cda820
47e4a42e61e6dc3ef78415bfc22f7c173097d693811e642532dd49e2e82c0c4b
6ef464d88b7956558e2e729ede88f3378d634d820bf9485cbc2e674cf665fd6b
170c66b73b7f1dc14770ccf83cd5e0140085d0dc6eafcf29a69f28ec22ea4fc0
*/
