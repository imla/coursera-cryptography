In an attempt to avoid the attacks on the "plain RSA" signature scheme, J. Random Hacker has designed her own RSA-based signature scheme.
The scheme works as follows:
The public key is a standard RSA public key (N, e), and the private key is the usual (N, d), where N is a 128-byte (1024-bit) integer.
To sign a message m of length exactly 63 bytes, set
        M = 0x00 m 0x00 m
(note that M is exactly 128 bytes long) and then compute the signature M^d mod N.
(If m is shorter than 63 bytes, 0-bytes are first preprended to make its length exactly 63 bytes.
This means that the signature on any message m is the same as the signatures on 0x00 m and 0x00 00 m, etc., allowing easy forgery attacks.
This is a known vulnerability that is not the point of this problem.)
J. Random Hacker is so sure this scheme is secure, she is offering a bounty of 1 point to anyone who can forge a signature on the 63-byte message:
        "Crypto is hard --- even schemes that look complex can be broken"
with respect to the public key:
N = 0xa99263f5cd9a6c3d93411fbf682859a07b5e41c38abade2a551798e6c8af5af0
      8dee5c7420c99f0f3372e8f2bfc4d0c85115b45a0abc540349bf08b251a80b85
      975214248dffe57095248d1c7e375125c1da25227926c99a5ba4432dfcfdae30
      0b795f1764af043e7c1a8e070f5229a4cbc6c5680ff2cd6fa1d62d39faf3d41d
and e = 0x10001.

You will be given the ability to obtain signatures on messages of your choice -except for the message above!-
You will also be given access to a verification routine that you can use to check your solution.
Note that we have not discussed the above signature scheme in the lectures.
But you have learned enough to enable you to find an attack on the scheme if you are sufficiently clever...
All the files needed for this assignment are available here, including a README file that explains everything.
Note that this assignment requires the ability to perform basic networking,
as well as to perform "big integer" arithmetic (exponentiation, modular reduction, etc.).
Because we do not assume students necessarily know this, we have provided stub code in Python for doing basic networking
along with some examples of how to carry out "big integer" arithmetic.
However, you are welcome to use any language of your choice to implement the attack.
(Students may feel free to post stub code in other languages on the discussion boards.)


NB: Python files are rewriten to c code using openssl (bignumber) 
